# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/mongodb/mongodbatlas" {
  version     = "1.0.2"
  constraints = "1.0.2"
  hashes = [
    "h1:k7bJc6t9EdVIOMjgRmdsD87boW1HhJlw7nxqgwYVXf8=",
    "zh:3c8707d326b4dd0f5289d97e4125218754610f9a73acd66a0ea0db0444e932b6",
    "zh:484356d87d6d22d99bb5b9be56cc716debd02f8347ab0a173f75c81ea12af485",
    "zh:61a471d3689c97504f9926b8bd33d3e7119831ec100d0aa3854c5b962a66b54c",
    "zh:62f8df440dc34219c9818f841499d3a92c74703dc8b9659549efa767824702e0",
    "zh:685e84acc3d47d97c66dd07459c96447e0d3c2db1aba4c756306aaa6c8388f9a",
    "zh:7796f1f3e4d4d8792c4c06aeb1155490d022e8fe0f645f8be94d0bac1b92520a",
    "zh:79b62b2f6f4222eac1897fc083876796a106bf24ee58333fab02feffdaad574e",
    "zh:8a0653b49df65bd37f38e7e6e3515976e0984a8f131df252980c77d209c1b52c",
    "zh:c15063456424024ca3cc7621471b6b05a2b39af66ca4bb4fcd9923eb9a10cde5",
    "zh:c24f57812d5795cc83e62466d67b61ed7de957111d7c70579aeca65a0e0a4d4d",
    "zh:c8be6c222dce3a9fb4f864f856f9881176c982ae18e32d741c027c147ca459bb",
    "zh:d16bdbdc13db937daf214d6c59a2f0f143048ac922276cd961a75f5f5d2a5542",
    "zh:d5a5a2b2528f1a38e1c96011cbf79f0fba03371d7d6183b999ee887368a1a0a5",
  ]
}
