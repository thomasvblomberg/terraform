# Terraform POC

This is my POC sandbox for Terraform.

## MongoDB Atlas

The first example I have is for creating a serverless cluster in Atlas. 

At the time of writing this the [mongodb/mongodbatlas](https://github.com/mongodb/terraform-provider-mongodbatlas) terraform plugin has this as a beta feature that you have to opt in for.

Listed below are the required variables to run this project.

| Variable                   | Description                      | Default      |
| -------------------------- | ---------------------------------- | ---------- |
| mongodbatlas_instance_name | The mongo serverless instance name | terra-test |
| mongodbatlas_project_id|The mongo atlas project id|
| mongodbatlas_public_key|The mongo atlas public key|
| mongodbatlas_private_key|The mongo atlas private key|

### Override Production Plugin
At the time of writing this serverless functionality was just added to the mongo terraform plugin. To use the serverless functionality I needed to pull down and build the plugin. I then could enable developer mode by pointing to my local trfc file.

 1. Checkout the repo https://github.com/mongodb/terraform-provider-mongodbatlas
 2. Run make build (you must have go installed)
 3. Enable the developer config file

The .trfc configuration assumes that your repo is one directory out from this repo.

├── Terraform

└── terraform-provider-mongodbatlas


    export TF_CLI_CONFIG_FILE=./.trfc

### Enable Beta
Serveless is a beta feature for the mongo terraform plugin. To be able to use it you need to opt in to beta features. You can do this by setting an environment variable.

    export MONGODB_ATLAS_ENABLE_BETA=true

### Create Serverless Instance
    terraform apply -var mongodbatlas_project_id="YOURPROJECTID" -var mongodbatlas_public_key="YOURPUBLICKEY" -var mongodbatlas_private_key="YOURPRIVATEKEY"

### Destroy
    terraform destroy -var mongodbatlas_project_id="YOURPROJECTID" -var mongodbatlas_public_key="YOURPUBLICKEY" -var mongodbatlas_private_key="YOURPRIVATEKEY"


