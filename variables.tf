
variable "mongodbatlas_instance_name" {
    description = "The mongo serverless instance name"
    type = string
    default = "terra-test"
}

variable "mongodbatlas_public_key" {
    description = "The mongo atlas public key"
    type = string
    default = ""
}

variable "mongodbatlas_private_key" {
    description = "The mongo atlas private key"
    type = string
    default = ""
}

variable "mongodbatlas_project_id" {
    description = "The mongo atlas project id"
    type = string
    default = ""
}